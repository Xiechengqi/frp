#!/usr/bin/env bash
# ---------------------------------
#
# author: xiechengqi
# function: auto install frp client on CentOS 7
# data: 2020/08/04
#
# ---------------------------------
# function RED(){echo -e "\033[31m"$@"\033[0m"}
# function GREEN(){echo -e "\033[32m"$@"\033[0m"}
[ $(id -u) != "0" ] && echo "Error: You must be root to run" && exit 1
hash curl || exit 1
curl -o /tmp/frp.tar.gz https://pan.xiechengqi.top/api/v3/file/get/24/frp.tar.gz?sign=SMFPRYlsQkxJAeYvH2ik3NPbXha7Ug-w8Q_lWCSZVzQ%3D%3A0 && tar zxvf /tmp/frp.tar.gz -C /tmp/ && echo '------------------------- Start install frp -----------------------------' && bash /tmp/frp/frp_install.sh
